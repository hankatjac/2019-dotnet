﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizard
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; } 
        public double Longitude { get; set; }

        public override string ToString()
        {
            return $"{Name}";
        }


    }
}
