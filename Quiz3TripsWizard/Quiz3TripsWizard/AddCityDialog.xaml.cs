﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz3TripsWizard
{
    
    /// <summary>
    /// Interaction logic for AddCityDialog.xaml
    /// </summary>
    public partial class AddCityDialog : Window
    {
        
        public AddCityDialog(Window owner)
        {          
            InitializeComponent();
            Owner = owner;
            //lvFromCity.Items.Add("Montreal");
            //lvFromCity.Items.Add("Toronto");
            //lvFromCity.Items.Add("New York"); 
            //lvToCity.Items.Add("Montreal");
            //lvToCity.Items.Add("Toronto");
            //lvToCity.Items.Add("New York");

            if (Globals.ctx.Cities.Count()== 0)
            {
                CityDataInitialization();
            }

            lvFromCity.ItemsSource = (from c in Globals.ctx.Cities select c).ToList<City>();
            lvToCity.ItemsSource = (from c in Globals.ctx.Cities select c).ToList<City>();
        }

        private void Next_ButtonClick(object sender, RoutedEventArgs e)
        {

            AddPersonDialog dialog = new AddPersonDialog(this);
            dialog.ShowDialog();
            DialogResult = true; // Close dialog with success result 
        }

        private void LvFromCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            City fromCity = lvFromCity.SelectedItem as City;
            if (fromCity == null)
            {
                return; // 
            }

            Globals.fromCity = fromCity;
            
        }

        private void LvToCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            City toCity = lvToCity.SelectedItem as City;
            if (toCity == null)
            {
                return; // 
            }
            Globals.toCity = toCity;
            //Globals.toCity = lvToCity.SelectedItem.ToString();
        }


        public static void CityDataInitialization()
        {
            City c1 = new City() { Name = "Montreal", Latitude = 45.5017, Longitude = 73.5673 };
            City c2 = new City() { Name = "Toronto", Latitude = 43.6532, Longitude = 79.3832 };
            City c3 = new City() { Name = "New York", Latitude = 40.7128, Longitude = 74.0060 };
            Globals.ctx.Cities.Add(c1);
            Globals.ctx.Cities.Add(c2);
            Globals.ctx.Cities.Add(c3);
            Globals.ctx.SaveChanges();
        }
    }
}
