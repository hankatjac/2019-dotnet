﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3TripsWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new TripDbContext();
                lvTrips.ItemsSource = (from t in Globals.ctx.Travellers select t).ToList<Traveller>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("fatal error: " + ex.Message);
            }
        }

        private void AddCity_MenuClick(object sender, RoutedEventArgs e)
        {
            AddCityDialog dialog = new AddCityDialog(this);
            dialog.ShowDialog();
        }


        private void Exit_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsCollection = lvTrips.SelectedItems;
            if (selectedItemsCollection.Count == 0)
            { // TODO: make MB nicer
                MessageBox.Show("Select some records first");
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text file (*.txt)|*.txt|Any file (*.*)|*.*";
            sfd.ShowDialog();
            if (sfd.FileName != "")
            {
                List<string> linesList = new List<string>();
                foreach (var item in selectedItemsCollection)
                {
                    //if (item is City)
                    //{
                    //    City city = item as City;
                    //    linesList.Add($"City{city.Id};{city.Name};{city.Latitude};{city.Longitude}");
                    //}
                    //else if (item is Traveller)
                    
                        Traveller traveller = item as Traveller ;
                        linesList.Add($"Traveller {traveller.Id};{traveller.PersonName};{traveller.FromCity};{traveller.ToCity};{traveller.DistanceKm};{traveller.DepartureDate};{traveller.ReturnDate}");
     
                }
                try
                {
                    File.WriteAllLines(sfd.FileName, linesList);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file:\n" + ex.Message);
                }
            }
        }

    }
}
