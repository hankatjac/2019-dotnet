﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz3TripsWizard
{
    /// <summary>
    /// Interaction logic for AddPersonDialog.xaml
    /// </summary>
    public partial class AddPersonDialog : Window
    {
        
        public AddPersonDialog(Window owner)
        {
            InitializeComponent();
            Globals.mw = (MainWindow)Application.Current.MainWindow;
            Owner = owner;
            lblFromCity.Content = Globals.fromCity.Name;
            lblToCity.Content = Globals.toCity.Name;
        }

        private void Finish_ButtonClick(object sender, RoutedEventArgs e)
        {
            string personName = tbName.Text;
            DateTime departureDate = dpDepartureDate.SelectedDate.Value;
            DateTime returnDate = dpReturnDate.SelectedDate.Value;
         

          
            try
            {

                // add
                Traveller traveller = new Traveller() { PersonName = personName, FromCity = Globals.fromCity, ToCity = Globals.toCity, DepartureDate = departureDate, ReturnDate = returnDate};

                Globals.ctx.Travellers.Add(traveller);
                Globals.ctx.SaveChanges();
              
            }
            catch (DataException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }
            catch (SystemException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }


            DialogResult = true; // Close dialog with success result

            Globals.mw.lvTrips.ItemsSource = (from t in Globals.ctx.Travellers select t).ToList<Traveller>();
        }


        //public static double DistanceBetweenTwoCities(double latitudeFromCity, double longitudeFromCity, double latitudeToCity, double longitudeToCity)
        //{
        //    var sCoord = new GeoCoordinate(latitudeFromCity, longitudeFromCity);
        //    var eCoord = new GeoCoordinate(latitudeToCity, longitudeToCity);
        //    return sCoord.GetDistanceTo(eCoord);
        //}

    }
}
