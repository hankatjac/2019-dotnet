﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizard
{
    class Globals
    {
        public static TripDbContext ctx;
        public static City fromCity;
        public static City toCity;
        public static MainWindow mw;
        public const string AppName = "Trip Entity";
    }
}
