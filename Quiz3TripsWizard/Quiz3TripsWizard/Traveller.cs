﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizard
{
    public class Traveller
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string PersonName { get; set; } // 1-50 characters
        public virtual City FromCity { get; set; }// may be null
        public virtual City ToCity { get; set; } // may be null
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public virtual double DistanceKm
        { 
            get
            {
                var sCoord = new GeoCoordinate(FromCity.Latitude, FromCity.Longitude);
                var eCoord = new GeoCoordinate(ToCity.Latitude, ToCity.Longitude);
                return sCoord.GetDistanceTo(eCoord)/1000;
            }

        }
       
    }

}
