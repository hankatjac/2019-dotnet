﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03ArrayContains
{
    class Program
    {

        public static void PrintDups(int[,] a1, int[,] a2)
        {
            

            Console.WriteLine("Duplicates: ");
            //foreach (int v1 in a1)
            //{
            //    foreach (int v2 in a2)
            //    {
            //        if (v1 == v2)
            //        {

            //            Console.Write($"{v1},");
            //        }

            //    }
            //}
            List<int> uniqueList = new List<int>();
            foreach (int v1 in a1)
            {
                foreach (int v2 in a2)
                {
                    if (v1 == v2)
                    {
                        if (!uniqueList.Contains(v1))
                        {
                            uniqueList.Add(v1);
                        }
                    }

                }
            }

            foreach (int v in uniqueList)
            {
                Console.Write($"{v} ");
            }

        }



        static void PrintTree(int size)
        {
            for (int line = 0; line<size; line++)
            {
                for (int row = 0; row< (size - line); row++)
                {
                    Console.Write(" ");
                }

                for (int row = 0; row < line; row++)
                {
                    Console.Write("*");
                }

                for (int row = 0; row < (line + 1); row++)
                {
                    Console.Write("*");
                }


                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            int[,] data1 = { { 2, 3 }, { 7, 8 }, { 3, 17 } };
            int[,] data2 = { { 64, 3, 7 }, { 1, 8 ,11} };

            PrintDups(data1, data2);
            Console.WriteLine();
           

            PrintTree(5);
            Console.ReadKey();
        }

      

    }
}
