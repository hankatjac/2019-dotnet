﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    class SuperFib<R>
    {
        private R[] fibArr = new R[100];


        public R this[int index]
        {
            get
            {
                if (index < 0 || index >= fibArr.Length)
                {
                    throw new IndexOutOfRangeException("Cannot store more than 10 objects");
                }
               

                return fibArr[index];
            }

            set
            {
                if (index < 0 || index >= fibArr.Length)
                    throw new IndexOutOfRangeException("Cannot store more than 10 objects");

                fibArr[index] = value;
            }

        }

    }
}
