﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException() { }
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception orig) : base(msg, orig) { }
    }

    class Airport
    {
        private string _code; // exactly 3 uppercase letters
        public String Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (value.Length !=3 || value.Contains(";"))
                {
                    throw new ArgumentException("Code must be exactly 3 uppercase letters, no semicolons");
                }
                _code = value.ToUpper();
            }
        }

        private string _city; // 1-50 characters, no semicolons
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new ArgumentException("City must be be 1-50 characters long, no semicolons");
                }
                _code = value;
            }
        }



        private double _latitude;// -90 to 90, -180 to 180
        public double Latitude // Age 0-150
        {
            get
            {
                return _latitude;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new ArgumentException("Latitude must be -90 - 90");
                }
                _latitude = value;
            }
        }

        private double  _longitude;
        public double Longitude // Age 0-150
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    throw new ArgumentException("Logitue must be -180-180");
                }
                _longitude = value;
            }
        }


        private int _elevationMeters; //-1000 to 10000
        public int ElevationMeters
        {
            get
            {
                return _elevationMeters;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("ElevationMeters must be -1000-10000");
                }
                _elevationMeters = value;
            }
        }

        public Airport(string code, string city, double lat, double lng, int elevM)
        {
            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            ElevationMeters = elevM;
        }
        public Airport(string line)
        {
            string[] data = line.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Line for Person must have exactly 5 values");
            }

            try
            {
                Code = data[0];
                City = data[1];
                Latitude = double.Parse(data[2]);
                Longitude = double.Parse(data[3]);
                ElevationMeters = int.Parse(data[4]);
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                { // exception chaining (translate one exception into another)
                    throw new InvalidDataException("Double or Integer value expected", ex);
                }
                else throw ex;
            }
        }

        public override string ToString()
        {
            return $"Airport: {Code}; {City}; {Latitude}; {Longitude}; {ElevationMeters}";
        }


        public string ToDataString()
        {
            return $"Airport {Code} is in city {City}, latitude {Latitude}, logitude {Longitude}, elevation meters {ElevationMeters}";
        }

    }
}
