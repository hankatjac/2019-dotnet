﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    class Program
    {
        const string DataFileName = @"..\..\airport.txt";

        static List<Airport> airportsList = new List<Airport>();


        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate logger;

        public static void ReadDataFromFile()
        {
            airportsList.Clear();
            /* if (!File.Exists(DataFileName))
            {
                return;
            } */
            try
            {
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (string line in linesArray)
                {
                    Airport airport = new Airport(line);
                    airportsList.Add(airport);
                }

            }
            catch (FileNotFoundException ex)// file not found is not an error
            {
                Console.WriteLine("File not found: " + ex.Message);
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine("Error parsing line: " + ex.Message);
            }

        }

        static void AddAirportInfo()
        {
            Console.WriteLine("Adding a airport.");
            Console.Write("Enter code: ");
            string code = Console.ReadLine();

            Console.Write("Enter city: ");
            string city = Console.ReadLine();

            Console.Write("Enter latitude: ");
            string latitudeStr = Console.ReadLine();
            if (!double.TryParse(latitudeStr, out double latitude))
            {
                Console.WriteLine("latitude must be a valid integer");
                return;
            }


            Console.Write("Enter longitude: ");
            string longitudeStr = Console.ReadLine();
            if (!double.TryParse(longitudeStr, out double longitude))
            {
                Console.WriteLine("longitude must be a valid integer");
                return;
            }

            Console.Write("Enter elevation in meters: ");
            string elevMStr = Console.ReadLine();
            if (!int.TryParse(elevMStr, out int elevM))
            {
                Console.WriteLine("elevation meter must be a valid integer");
                return;
            }
          
            try
            {
                Airport a = new Airport( code,city,latitude,longitude,elevM ) ;
                airportsList.Add(a);
                Console.WriteLine("aiport added.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("airport data invalid: " + ex.Message);
            }
        }


        static void ListAllAirportsInfo()
        {
            foreach (Airport a in airportsList)
            {
                Console.WriteLine(a);
            }
        }

        static void FindAirportByCode() // IMPLEMENT USING LINQ
        {
            Console.WriteLine("Enter partial code to look for: ");
            string code = Console.ReadLine().ToUpper();
            var matchList = from a in airportsList where a.Code.ToUpper().Contains(code) select a;
            Console.WriteLine("Matching airports: ");
            foreach (var m in matchList)
            {
                Console.WriteLine(m);
            }
        }

        static void FindairportElevationStandarddeviation()
        {
          
            // compute average
            double avg = airportsList.Average(s => s.ElevationMeters);
            Console.WriteLine($"Average Elevation Meters is: {avg}");

            // compute stdandard deviation
            List<int> ElevationMetersList = (from s in airportsList select s.ElevationMeters)
                                    .ToList<int>();
            double sumOfSquares = 0;
            foreach (double elevationMeters in ElevationMetersList)
            {
                sumOfSquares += (elevationMeters - avg) * (elevationMeters - avg);
            }
            double stddev = Math.Sqrt(sumOfSquares / ElevationMetersList.Count());
            Console.WriteLine($"Standard deviation is {stddev:0.00}");
        }


        static void PrintNthsuperFibonacci(int n)
        {
            int a = 0, b = 1, c = 1;
            int d = 0;

            SuperFib<int> sf = new SuperFib<int>();
            for (int i = 0; i < n; i++)
            {
                d = a + b + c;
                a = b;
                b = c;
                c = d;
               
            }
            Console.WriteLine($"value is {d}");
        }

  


        public static void WriteDataToFile()
        {
            List<string> linesList = new List<string>();
            foreach (var a in airportsList)
            {
                linesList.Add(a.ToDataString());
            }
            try
            {
                File.WriteAllLines(DataFileName, linesList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error wirting to file " + ex.Message);
            }
        }


        //- methods for logging
        public static void LogToConsole(string m)
        {
            Console.WriteLine("Screen: " + m);
        }
        public static void LogToFile(string message)
        {
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            File.AppendAllText(@"..\..\events.txt", $"{now}: {message}{Environment.NewLine}");
            
        }


        static int GetMenuChoice()
        {
            Console.Write(
            @"
            1. Add Airport
            2. List all airports
            3. Find nearest airport by code (preferably using LINQ)
            4. Find airport's elevation standard deviation (using LINQ)
            5. Print Nth super-Fibonacci
            6. Change log delegates
            0. Exit
            Enter your choice: ");
            try
            {
                string choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("Invalid value entered\n");
                }
                else throw ex; // if we don't handle an exception we MUST throw it!
            }
            return -1;
        }

      


        static void Main(string[] args)
        {
            LoggerDelegate logger = null;
            ReadDataFromFile();
            int choice;
            do
            {
                choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddAirportInfo();                        
                        logger = LogToConsole;
                        logger += LogToFile;
                       
                        break;
                        
                    case 2:
                        ListAllAirportsInfo();
                        logger = LogToConsole;
                        logger += LogToFile;
                       
                        break;
                    case 3:
                        FindAirportByCode();
                        logger = LogToConsole;
                        logger += LogToFile;
                        
                        break;
                    case 4:
                        FindairportElevationStandarddeviation();
                        logger = LogToConsole;
                        logger += LogToFile;
                       
                        break;
                    case 5:
                        Console.WriteLine("Which Nth super-Fibonacci value would you like to see?");
                        try
                        {
                            int n = int.Parse(Console.ReadLine());
                            PrintNthsuperFibonacci(n);
                        }
                        catch (Exception ex)
                        {
                            if (ex is FormatException | ex is OverflowException)
                            {
                                Console.WriteLine("Invalid value entered\n");
                            }
                            else throw ex; // if we don't handle an exception we MUST throw it!
                        }
                        logger = LogToConsole;
                        logger += LogToFile;
                    

                        break;
                    case 6:
                        Console.WriteLine("Enter your choices, comma-separated, empty for none:1,2?");
                        try
                        {
                            int n = int.Parse(Console.ReadLine());
                            if (n< 1 ||n > 2)
                            {
                                throw new ArgumentException("Logitude must be 1 or 2");
                            }
                            logger = null;
                            if ( n == 1 )
                            {
                                logger = LogToConsole;
                                logger?.Invoke("log starts");
                            }

                            if (n == 2)
                            {
                                logger = LogToConsole;
                                logger += LogToFile;
                                logger?.Invoke("log starts");
                            }

                            logger?.Invoke("begin to log");
                        }
                        catch (Exception ex)
                        {
                            if (ex is FormatException | ex is OverflowException)
                            {
                                Console.WriteLine("Invalid value entered\n");
                            }
                            else throw ex; // if we don't handle an exception we MUST throw it!
                        }

                        break;
                    case 0: // exit
                        Console.WriteLine("Good bye.");
                        break;
                    default: // ALWAYS have a default handler in switch/case
                        Console.WriteLine("Invalid choice try again.");
                        break;
                }
                Console.WriteLine();
            } while (choice != 0);
            WriteDataToFile();

        }
    }
}
