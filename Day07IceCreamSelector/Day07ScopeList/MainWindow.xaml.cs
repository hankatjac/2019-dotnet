﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07ScopeList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FlavorAdd_ButtonClick(object sender, RoutedEventArgs e)
        {
            //method one
            //var item = lvFlavorAvailable.SelectedItem as ListViewItem;
            //lvFlavorSelected.Items.Add(item.Content);

            // method two
            if (lvFlavorAvailable.SelectedIndex == -1)
            {
                lblErrorAvailable.Visibility = Visibility.Visible;
            }
            //var item = lvFlavorAvailable.SelectedItem as ListViewItem;
            //ListViewItem newItem = new ListViewItem
            //{
            //    Content = item.Content
            //};
            //lvFlavorSelected.Items.Add(newItem.Content);

            var selectedItemsCollection = lvFlavorAvailable.SelectedItems;
            foreach ( var item in selectedItemsCollection)
            {
                ListViewItem lvi = item as ListViewItem;
                lvFlavorSelected.Items.Add(lvi.Content);
            }

        }

        private void RemoveSelected_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (lvFlavorSelected.SelectedIndex == -1)
            {  
                lblErrorAvailable.Visibility = Visibility.Visible;
            }
            lvFlavorSelected.Items.RemoveAt(lvFlavorSelected.Items.IndexOf(lvFlavorSelected.SelectedItem));
        }

        private void RemovAll_ButtonClick(object sender, RoutedEventArgs e)
        {
            lvFlavorSelected.Items.Clear();
            lblErrorAvailable.Visibility = Visibility.Hidden;
        }
    }
}
