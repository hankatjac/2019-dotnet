﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04StudentsGrades
{
    class Program
    {

        const string DataFileName = @"..\..\grades.txt";
        static List<Student> studentsList = new List<Student>();

        static double LetterToNumberGrade(string grade)
        {
            double gpa = 0;
            switch (grade)
            {
                case "A+": gpa = 4.33; break;
                case "A": gpa = 4.00; break;
                case "A-": gpa = 3.67; break;
                case "B+": gpa = 3.33; break;
                case "B": gpa = 3; break;
                case "B-": gpa = 2.67; break;
                case "C+": gpa = 2.33; break;
                case "C": gpa = 1.00; break;
                case "C-": gpa = 1.67; break;
                case "D+": gpa = 1.33; break;
                case "D": gpa = 1.00; break;
                case "D-": gpa = 0.67; break;
                case "F": gpa = 0; break;
                default:
                    Console.WriteLine("Not found the accordingly grade");
                    break;
            }
            return gpa;

        }
        static void Main()
        {
            try
            {
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (string line in linesArray)
                {


                    string[] data = line.Split(':');
                    if (data.Length < 2)
                    {
                        Console.WriteLine("Error: invalid number of fields in line: " + line);
                        continue;
                    }
                    string name = data[0];
                    string[] grades = data[1].Split(',');
                    //double[] gpa = new double[grades.Length];
                    //for (int i = 0; i < grades.Length; i++)
                    //{
                    //    gpa[i] = LetterToNumberGrade(grades[i]);

                    //}
                    //double avg = gpa.Average();

                    List<double> gpaList = new List<double>();
                    foreach(string grade in grades)
                    {
                        gpaList.Add(LetterToNumberGrade(grade));
                    }
                    double avg = gpaList.Average();

                    // create Student, add to list
                   
                        Student s = new Student { Name = name, GPA = avg };
                        studentsList.Add(s);
                  
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("Error file not found " + ex.Message);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading from file " + ex.Message);
            }

            foreach (Student s in studentsList)
            {
                Console.WriteLine(s);
            }
            Console.ReadKey();
        }
    }
}
