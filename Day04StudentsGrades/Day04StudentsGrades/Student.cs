﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04StudentsGrades
{
    class Student
    {
        public string Name { get; set;}
        public double GPA{ get; set;}

        public override string ToString()
        {
            return $"{Name} has GPA {GPA:0.00}";
        }

        // serialization of data / marchalling
        public string ToDataString()
        {
            return $"{Name};{GPA}";
        }
    }
}
