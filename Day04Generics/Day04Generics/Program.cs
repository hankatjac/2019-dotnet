﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            Box<string> boxOfString = new Box<string>();
            boxOfString.value = "abc";
            Console.WriteLine("Value is: " + boxOfString.value);

            BoxTwoCompartments<double, string> twoComp = new BoxTwoCompartments<double, string>();
            twoComp.compA = 234.343;
            twoComp.compB = "Jimmy";

        }
    }
}
