﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04Generics
{
    class BoxTwoCompartments<M,N>
    {
        public M compA;
        public N compB;

        public M getCompA()
        {
            M val= compA;
            return val;
        }


        public void setCompA(M val)
        {
            compA = val;
        }
    }
}
