﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02FirstLinq
{
    class Program
    {
        static List<string> nameList = new List<string>();

        static void Main(string[] args)
        {
            try
            {


                while (true)
                {
                    Console.Write("Enter Name:");
                    String name = Console.ReadLine();
                    if (name == " ")
                    {
                        break;
                    }

                    nameList.Add(name);
                }

                //       
                Console.Write("Enter serach string:");
                string search = Console.ReadLine().ToUpper();
                //var foundList = from n in nameList where n.ToUpper().Contains(search) select n;
                var foundList = nameList.Where(n => n.ToUpper().Contains(search));

                Console.WriteLine("matiching name");
                foreach (string name in foundList)
                {
                    Console.WriteLine(name);
                }

                // sort names alphabetically using LINQ and print them out one per line

                //create a query that obtain the values in sorted order
                var  sortList = from n in nameList  orderby n select n;
                //var sortList = nameList.OrderBy(n => n);

                Console.WriteLine("Values in ascending order: ");

                // Execute the query and display the results.

                foreach (string name in sortList)
                {
                    Console.Write(name + " \n");
                }

                var sortListDesc = from n in sortList orderby n descending select n;
                

                Console.WriteLine("Values in ascending order: ");

                // Execute the query and display the results.

                foreach (string name in sortListDesc)
                {
                    Console.Write(name + " \n");
                }

            } finally
            {
                Console.WriteLine("Press a key to finish");
                Console.ReadKey();
            }



        }
    }
}
