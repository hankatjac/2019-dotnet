﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08FriendsDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Friend> friendsList = new List<Friend>();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                lvFriends.ItemsSource = Globals.Db.GetAllFriends();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
                Close();
            }
        }

        private void AddUpdateFriend_ButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            bool isUpdating = (button.Name == "btUpdateFriend");
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            if (!int.TryParse(ageStr, out int age))
            {
                MessageBox.Show("Age must be and integer");
                return;
            }
           
            try
            {
                if (isUpdating)
                {
                    Friend friend = lvFriends.SelectedItem as Friend;
                    if (friend == null) return; // should nvever happen - internal error
                    friend.Name = name;
                    friend.Age = age;
                    Globals.Db.UpdateFriend(friend);
                }
                else // adding
                {
                    Friend friend = new Friend() { Name = name, Age = age };  
                    Globals.Db.AddFriend(friend);
                }
                tbName.Text = "";
                tbAge.Text = "";
                lvFriends.ItemsSource = Globals.Db.GetAllFriends();
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex);
            }
        }



        private void DeleteFriend_ButtonClick(object sender, RoutedEventArgs e)
        {
            Friend friend = lvFriends.SelectedItem as Friend;
            if (friend == null) return;
            MessageBoxResult result =  MessageBox.Show("Really?", Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeleteFriend(friend.Id);
                    lvFriends.ItemsSource = Globals.Db.GetAllFriends();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Database error:\n" + ex);
                }
            }
        }

        private void LvFriends_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Friend friend = lvFriends.SelectedItem as Friend;
            if (friend == null)
            {
                btUpdateFriend.IsEnabled = false;
                btDeleteFriend.IsEnabled = false;
                return; // disable update and delete buttons
            }
            // enable update and delete buttons, load data
            btUpdateFriend.IsEnabled = true;
            btDeleteFriend.IsEnabled = true;
            lblId.Content = friend.Id;
            tbName.Text = friend.Name;
            tbAge.Text = friend.Age + "";

        }

        private void FileExportSelected_MenuItemClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsCollection = lvFriends.SelectedItems;
            if (selectedItemsCollection.Count == 0)
            { // TODO: make MB nicer
                MessageBox.Show("Select some records first");
            }

            SaveFileDialog sfd = new SaveFileDialog() { Filter = "Text file (*.txt)|*.txt|Any file (*.*)|*.*" };
           
            sfd.ShowDialog();
            if (sfd.FileName != "")
            {
                List<string> linesList = new List<string>();
                foreach (var item in selectedItemsCollection)
                {
                    Friend f = item as Friend;
                    linesList.Add($"{f.Id};{f.Name};{f.Age}");
                }
                try
                {
                    File.WriteAllLines(sfd.FileName, linesList);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file:\n" + ex.Message);
                }
            }
        }
    }
}
