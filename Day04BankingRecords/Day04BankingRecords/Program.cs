﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04BankingRecords
{
    class Program
    {
        static List<AccountTransaction> transactionList = new List<AccountTransaction>();
        const string fileName = @"..\..\operations.txt";
        static void Main(string[] args)
        {
            transactionList.Clear();
            try
            { 
                string[] lines = File.ReadAllLines(fileName);
                foreach (string line in lines)
                {
                    AccountTransaction a = new AccountTransaction(line);
                    transactionList.Add(a);
                } 
                foreach(var a in transactionList)
                {
                    Console.WriteLine(a.ToDataString());
                }
                
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("File not found" + ex);
            }
            catch (IOException e)
            {
                Console.WriteLine("Reading File Error" + e);
            }

            Console.ReadKey();
        }
    }
}
