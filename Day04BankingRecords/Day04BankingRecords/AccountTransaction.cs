﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04BankingRecords
{
    public class InvalidParameterException : Exception
    {
        public InvalidParameterException() { }
        public InvalidParameterException(string msg) : base(msg) { }
        public InvalidParameterException(string msg, Exception orig) : base(msg, orig) { }
    }
    class AccountTransaction
    {
     

        DateTime Date { get; }
        string Description { get; }
        decimal Deposit { get; }
        decimal Withdrawal { get; }

        public AccountTransaction(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 4)
            {
                throw new InvalidParameterException("Line for Person must have exactly 3 values");
            }

            try
            {
                Date = DateTime.ParseExact(data[0], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Description = data[1];
                Deposit = decimal.Parse(data[2]);
                Withdrawal = decimal.Parse(data[3]);


                if ((Deposit == 0 && Withdrawal == 0) || (Deposit < 0 || Withdrawal < 0))
                {
                    throw new InvalidParameterException("not valid data");
                }

            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                { // exception chaining (translate one exception into another)
                    throw new InvalidParameterException("Integer value expected", ex);
                }
                else throw ex;
            }
        }
        public  string ToDataString()
        {
            return $"transaction;{Date};{Description};{Deposit:0.00};{Withdrawal:0.00}";
        }

        public override string ToString()
        {
            return $"transaction: {Date} {Description} {Deposit:0.00} {Withdrawal:0.00} GPA";
        }
    

    }
}
