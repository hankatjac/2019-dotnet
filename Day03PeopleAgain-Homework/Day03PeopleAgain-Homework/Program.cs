﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain_Homework
{
    class Program
    {

        const string DataFileName = @"..\..\people.txt";
        const string DataFileName1 = @"..\..\people1.txt";
        static List<Person> peopleList = new List<Person>();
        static void ReadAllPeopleFromFile()
        {
            peopleList.Clear();
            try
            {
                //if(!File.Exists(DataFileName))
                //{
                //    return;
                //}
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (string line in linesArray)
                {

                    string[] data = line.Split(';');

                    if (data.Length == 3 && data[0] == "Person")
                    {
                 
                        string name = data[1];
                        string ageStr = data[2];

                        // parse the age
                        if (!int.TryParse(ageStr, out int age))
                        {
                            Console.WriteLine("Error: failed to parse age in line: " + line);
                            continue;
                        }



                        // create person, add to list

                        try
                        {
                            Person p = new Person(name, age);

                            peopleList.Add(p);
                            Console.Write("Peson added: ");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine("Person data invalid" + ex.Message);
                        }

                        Console.WriteLine($"{name} {ageStr}");
                    }

                    if (data.Length == 5 && data[0] == "Teacher")
                    {

                        string name = data[1];
                        string ageStr = data[2];
                        string subject = data[3];
                        string yoeStr = data[4];
                        // parse the age
                        if (!int.TryParse(ageStr, out int age))
                        {
                            Console.WriteLine("Error: failed to parse age in line: " + line);
                            continue;
                        }

                        if (!int.TryParse(yoeStr, out int yoe))
                        {
                            Console.WriteLine("Error: failed to parse age in line: " + line);
                            continue;
                        }


                        // create person, add to list

                        try
                        {
                            Person t = new Teacher(name, age, subject,yoe);

                            peopleList.Add(t);
                            Console.Write("Teacher added: ");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine("Teacher data invalid" + ex.Message);
                        }

                        Console.WriteLine($"{name} {ageStr} {subject} {yoeStr}");
                    }

                    if (data.Length == 5 && data[0] == "Student")
                    {

                        string name = data[1];
                        string ageStr = data[2];
                        string program = data[3];
                        string gpaStr = data[4];
                        // parse the age
                        if (!int.TryParse(ageStr, out int age))
                        {
                            Console.WriteLine("Error: failed to parse age in line: " + line);
                            continue;
                        }

                        if (!double.TryParse(gpaStr, out double gpa))
                        {
                            Console.WriteLine("Error: failed to parse age in line: " + line);
                            continue;
                        }


                        // create person, add to list

                        try
                        {
                            Person s = new Student(name, age, program, gpa);

                            peopleList.Add(s);
                            Console.Write("Student added: ");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine("Student data invalid" + ex.Message);
                        }

                        Console.WriteLine($"{name} {ageStr} {program} {gpa}");
                    }


                }
            }


            catch (FileNotFoundException ex)
            {
                //ignore
            }
            catch (IOException ex)
            {
                Console.WriteLine("reading from file" + ex.Message);
            }
        }

        static void ListAllPersonsInfo()
        {
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }
        }


        static void ListAllTeacher()
        {
            foreach (Person p in peopleList)
            {
                if (p.GetType().Equals(typeof(Teacher)))
                {
                    Console.WriteLine(p);
                }
            }
        }


        static void ListAllStudent()
        {
            foreach (Person p in peopleList)
            {
                if (p is Student)
                {
                    Console.WriteLine(p);
                }
            }
        }

        static void ListAllPersonExceptStudentAndTeacher()
        {
            foreach (Person p in peopleList)
            {
                if (p.GetType().Equals(typeof(Person)))
                {
                    Console.WriteLine(p);
                }
            }
        }


        static void StudentGPA()
        {
            int count=0;
            double sum=0;
            foreach (Person p in peopleList)
            {
                if (p is Student s)
                {
                    count += 1;
                    sum += s.GPA;                  
                }            
            }
            Console.WriteLine($"student average GPA is {sum / count}");

        }



        static void SaveAllPeopleToFile()
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (var p in peopleList)
                {
                    linesList.Add(p.ToDataString());
                    if (p is Student s)
                    {
                        linesList.Add(s.ToDataString());
                    }
                    if (p is Teacher t)
                    {
                        linesList.Add(t.ToDataString());
                    }
                }

                File.WriteAllLines(DataFileName1, linesList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Person saving to file" + ex.Message);
            }
        }



        public delegate void LoggerDelegate(string msg);

        public static void LogToScreen(string m)
        {
            Console.WriteLine("Screen: " + m);
        }


        public static void LogToFile(string message)
        {
            //DateTime dt = DateTime.Now;
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            File.AppendAllText(@"..\..\log.txt", $"{DateTime.Now}: {message} {Environment.NewLine}");
        }

        static int GetMenuChoice()
        {
           
            Console.Write(
                @"Where would you like to log setters errors?
                1-screen only
                2-screen and file
                3-do not log
                Your choice:");

            try
            {
                String choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }


            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("invalid value entered\n");

                }
                else throw ex; // if we don't handle and exception we must throw it!
            }

            return -1;
        }


        static void Main(string[] args)
        {

            ReadAllPeopleFromFile();
            ListAllPersonsInfo();
            ListAllTeacher();
            ListAllStudent();
            ListAllPersonExceptStudentAndTeacher();
            StudentGPA();
            SaveAllPeopleToFile();

           

            LoggerDelegate logger = null;
            int choice;
            do
            {
                choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        logger = LogToScreen; ;
                        break;
                    case 2:
                        logger = LogToScreen;
                    
                        logger += LogToFile; ;
                        break;
                    case 3:
                        logger = null;
                        break;
                   
                    default:
                        Console.WriteLine("invalid choice try again");
                        break;

                }
                Console.WriteLine();
            } while (choice != 0);








            Console.ReadKey();

        }
    }
}
