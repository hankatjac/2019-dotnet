﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain_Homework
{
    class Student :Person
    {

        private string _program; // 1-50 characters, no semicolons
        private double _gpa; // 0-4.3
      
        public Student(string name, int age, string program, double gpa): base (name, age )
        {
            _program = program;
            _gpa = gpa;
        }

      

        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length <= 2 || value.Length >= 100 || value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  2- 100 characters long, cannot contain semicolons");
                }
                _program = value;
            }
        }

        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value <= 0 || value >= 4.3)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _gpa = value;
            }
        }


        public override string ToString()
        {
            return $"{Name} is {Age} studying {Program} with GPA {GPA}";
        }

        public override string ToDataString()
        {
            return $"{Name};{Age};{Program};{GPA}";
        }


    }
}
