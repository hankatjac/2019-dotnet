﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain_Homework
{
    class Teacher :Person
    {


        private string _subject; // 1-50 characters, no semicolons
        private int _yearsOfExperience; // 0-100
      

       public Teacher(string name, int age, string subject, int yearOfExperience) : base(name, age)
        {
            _subject = subject;
            _yearsOfExperience = yearOfExperience;
        }

        public Teacher(string dataline) : base(dataline)
        {
            
        }


        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length <= 2 || value.Length >= 100 || value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  2- 100 characters long, cannot contain semicolons");
                }
                _subject = value;
            }
        }

        public int YOE
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if (value <= 0 || value >= 100)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _yearsOfExperience = value;
            }
        }


        public override string ToString()
        {
            return $"{Name} is {Age} teaching {Subject} with year of experience {YOE}";
        }


        public override string ToDataString()
        {
            return $"{Name};{Age};{Subject};{YOE}";
        }





    }
}
