﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03DelegateLogs
{
    class Program
    {

        public delegate void LoggerDelegate(string msg);

        public static void LogToScreen(string m)
        {
            Console.WriteLine("Screen: " + m);
        }

        public static void LogFancy(string mm)
        {
            Console.WriteLine("FAAAAAAAAAAAAAAANCY: " + mm);
        }

        public static void LogToFile(string message)
        {
            //DateTime dt = DateTime.Now;
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            File.AppendAllText(@"..\..\log.txt", $"{DateTime.Now}: {message} {Environment.NewLine}");
        }

        static void Main(string[] args)
        {
            try
            {
                LoggerDelegate logger = null;
                logger = LogToScreen;
                logger += LogFancy;
                logger += LogToFile;

                logger?.Invoke("Something happened");// nullable expression
                /*if (logger != null) logger("Something happened");*/ // if null
            }finally
            
            {
                Console.WriteLine("Press any key to finish"); 
                Console.ReadKey();
            }
        }
    }
}
