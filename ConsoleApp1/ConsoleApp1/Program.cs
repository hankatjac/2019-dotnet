﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    

    public class StringConversion
    {

        static double SafeDivision(double x, double y)
        {
            if (y == 0)
                throw new System.DivideByZeroException();
            return x / y;
        }

        public static void Main()
        {
            string input = String.Empty;
            try
            {
                int result = Int32.Parse(input);
                Console.WriteLine(result);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Unable to parse '{input}'");
            }
            // Output: Unable to parse ''

            try
            {
                int numVal = Int32.Parse("-105");
                Console.WriteLine(numVal);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            // Output: -105

            if (Int32.TryParse("-105", out int j))
                Console.WriteLine(j);
            else
                Console.WriteLine("String could not be parsed.");
            // Output: -105

            try
            {
                int m = Int32.Parse("abc");
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            // Output: Input string was not in a correct format.

            string inputString = "abc";
            if (Int32.TryParse(inputString, out int numValue))
                Console.WriteLine(inputString);
            else
                Console.WriteLine($"Int32.TryParse could not parse '{inputString}' to an int.");
            // Output: Int32.TryParse could not parse 'abc' to an int.

            double a = 98, b = 0;
            double r = 0;

            try
            {
                r= SafeDivision(a, b);
                Console.WriteLine("{0} divided by {1} = {2}", a, b, r);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Attempted divide by zero.");
            }

            Console.ReadKey();
        }
    }
}
