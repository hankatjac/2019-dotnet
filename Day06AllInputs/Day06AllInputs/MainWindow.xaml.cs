﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Register_ButtonClick(object sender, RoutedEventArgs e)
        {
            const string DataFileName = @"..\..\register.txt";

            List<string> inputList = new List<string>();


            inputList.Add(tbName.Text);

            if (rbBlow.IsChecked == true)
            {
                inputList.Add(rbBlow.Content.ToString());
            }
            else if (rbMiddle.IsChecked == true)
            {
                inputList.Add(rbMiddle.Content.ToString());
            }
            else if (rbUp.IsChecked == true)
            {
                inputList.Add(rbUp.Content.ToString());
            }
            else
            {
                // Internal Error
                MessageBox.Show("Internal error");
                return;
            }

            List<string> animalsList = new List<string>();
            if (cbCat.IsChecked == true)
            {
                animalsList.Add("Cat");
            }
            if (cbDog.IsChecked == true)
            {
                animalsList.Add("Dog");
            }
            if (cbOther.IsChecked == true)
            {
                animalsList.Add("Other");
            }
            string animals = String.Join(",", animalsList);
            inputList.Add(animals);
            //
            string continent = cmbContinent.Text;
            inputList.Add(continent);

            double tempC = slTempC.Value;
            //inputList.Add(tempC.ToString());

            // save to file
            try
            {
                File.WriteAllLines(DataFileName, inputList);
            }
            catch (IOException)
            {
                MessageBox.Show("Value must be numerical", "tempconv", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }
    }
}
