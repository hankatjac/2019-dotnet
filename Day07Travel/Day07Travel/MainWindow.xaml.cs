﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Travel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Trip> tripsList = new List<Trip>();
        public MainWindow()
        {
            InitializeComponent();
            lvTrips.ItemsSource = tripsList;  
        }

        private void AddTrip_ButtonClick(object sender, RoutedEventArgs e)
        {
            string destination = tbDestination.Text;
            string name = tbName.Text;
            string passport = tbPassPort.Text;
            try
            {
                DateTime departureDate = dpDeparture.SelectedDate.Value.Date;
                DateTime returnDate = dpReturn.SelectedDate.Value.Date;
          
                if (departureDate == null || returnDate == null) return;

                else if (returnDate <= departureDate)
                    MessageBox.Show("departure date must be earlier than return date");

                if (Trip.IsDestinationValid(destination) && Trip.IsNameValid(name) && Trip.IsPassportValid(passport) && Trip.IsDateValid(departureDate, returnDate))
                {
                    Trip t = new Trip()
                    {
                        Destination = destination,
                        Name = name,
                        Passport = passport,
                        DepartureDate = departureDate,
                        ReturnDate = returnDate
                    };
                    tripsList.Add(t);
                    lvTrips.Items.Refresh();
                    tbDestination.Text = "";
                    tbName.Text = "";
                    tbPassPort.Text = "";
                    dpDeparture.SelectedDate = null;
                    dpReturn.SelectedDate = null;
                }
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("date must not empty");
            }
        }
           

        
    }
}
