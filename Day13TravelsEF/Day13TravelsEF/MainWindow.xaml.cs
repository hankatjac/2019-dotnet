﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day13TravelsEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new TravelDbContext();
                lvTrains.ItemsSource = (from t in Globals.ctx.Trains select t).ToList<Train>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("fatal error: " + ex.Message);
            }

        }

        private void AddUpdateTrain_ButtonClick(object sender, RoutedEventArgs e)
        {

            Button button = sender as Button;
            bool isUpdating = (button.Name == "btUpdateTrain");

            
            string numberStr = tbNumber.Text;

            if (!int.TryParse(numberStr, out int number))
            {
                MessageBox.Show("Number must be and integer");
                return;
            }
          
            DateTime date = dpDate.SelectedDate.Value;

            try
            {
                if (isUpdating)
                {
                    Train train = lvTrains.SelectedItem as Train;
                    if (train == null) return; // should nvever happen - internal error
                    train.Number = number;
                    train.Date = date;
                    Globals.ctx.SaveChanges();  
                }
                else // adding
                {
                    Train train = new Train { Number = number, Date = date };
                    Globals.ctx.Trains.Add(train);
                    Globals.ctx.SaveChanges(); 
                }
                tbNumber.Text = "";
                dpDate.SelectedDate = DateTime.Now;
                lvTrains.ItemsSource = (from t in Globals.ctx.Trains select t).ToList<Train>();
            }
            catch (DataException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }
            catch (SystemException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }

        }
        private void DeleteTrain_ButtonClick(object sender, RoutedEventArgs e)
        {
            Train train = lvTrains.SelectedItem as Train;
            if (train == null) return; // should never happen
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n" + train, Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.ctx.Trains.Remove(train);
                    Globals.ctx.SaveChanges();
                    lvTrains.ItemsSource = (from t in Globals.ctx.Trains select t).ToList<Train>();
                }
                catch (DataException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
                catch (SystemException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
            }
        }

        private void LvTrains_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Train train = lvTrains.SelectedItem as Train;
            if (train == null)
            {
                // disable update and delete buttons
                btUpdateTrain.IsEnabled = false;
                btDeleteTrain.IsEnabled = false;
                return;
            }
            // enable update and delete buttons, load data
            btUpdateTrain.IsEnabled = true;
            btDeleteTrain.IsEnabled = true;
            lblId.Content = train.Id;
            tbNumber.Text = train.Number + "";
            dpDate.SelectedDate = train.Date;
        }

        private void AddPassenger_ButtonClick(object sender, RoutedEventArgs e)
        {

            PassengerDialog dialog = new PassengerDialog(this);
            dialog.ShowDialog();
        }

        private void LvTrains_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Train train = lvTrains.SelectedItem as Train;
            if (train == null) return;
            PassengerInfo dialog = new PassengerInfo(this, train);
            dialog.ShowDialog();
        }
    }
}
