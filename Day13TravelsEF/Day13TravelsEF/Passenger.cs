﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13TravelsEF
{
    public class Passenger
    {
        public int Id { get; set; }
        public string Name { get; set; } // 1-50 characters
        public Gender Gender { get; set; }
        public Train Train { get; set; } // may be null

        public override string ToString()
        {
            return $"Passenger{Id} {Name} {Gender} Train Id: {Train.Id}";
        }


    }

    public  enum Gender { Male = 1, Female = 2, NA = 3 };
}
