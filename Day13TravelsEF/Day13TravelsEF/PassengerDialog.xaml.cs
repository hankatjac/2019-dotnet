﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day13TravelsEF
{
    /// <summary>
    /// Interaction logic for AddEditPassengerDialog.xaml
    /// </summary>
    public partial class PassengerDialog : Window
    {

        Passenger currentlyEditedPassenger;
        public PassengerDialog(MainWindow owner)
        {
            InitializeComponent();
            try
            {
                lvPassengers.ItemsSource = (from p in Globals.ctx.Passengers select p).ToList<Passenger>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("fatal error: " + ex.Message);
            }

            Owner = owner;
            //currentlyEditedPassenger = passenger;
            foreach( Train train in Globals.ctx.Trains)
            {
                cmbTrain.Items.Add(train.Id);
            }

            //if (passenger != null)
            //{
            //    lblId.Content = passenger.Id;
            //    tbName.Text = passenger.Name;
            //    cmbGender.Text = passenger.Gender.ToString();
            //}
        }

        private void Save_ButtonClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            Gender gender = (Gender)Enum.Parse(typeof(Gender), cmbGender.Text);
            string trainIdStr = cmbTrain.Text;
            if (!int.TryParse(trainIdStr, out int trainId))
            {
                MessageBox.Show("erro: invalid Train Id");
                return;
            }

            var train = (from t in Globals.ctx.Trains where t.Id == trainId select t).FirstOrDefault<Train>();

            try
            {
                if (currentlyEditedPassenger != null)
                { // update
                    currentlyEditedPassenger.Name = name;
                    currentlyEditedPassenger.Gender = gender;
                    currentlyEditedPassenger.Train = train;
                    Globals.ctx.SaveChanges();
                }
                else
                { // add
                    Passenger passenger = new Passenger() { Name = name, Gender = gender, Train = train };
                    Globals.ctx.Passengers.Add(passenger);
                    Globals.ctx.SaveChanges();
                }
                lblId.Content = "-";
                tbName.Text = "";
                cmbGender.Text = "";
                cmbTrain.Text = "";
                lvPassengers.ItemsSource = (from p in Globals.ctx.Passengers select p).ToList<Passenger>();
            }
            catch (DataException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }
            catch (SystemException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
            }

            
        }

        private void LvPassengers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Passenger passenger = lvPassengers.SelectedItem as Passenger;
            currentlyEditedPassenger = passenger;
            // enable update and delete buttons, load data
            try
            {


                if (passenger == null)
                {
                    return;
                }
                lblId.Content = passenger.Id;
                tbName.Text = passenger.Name;
                cmbGender.Text = passenger.Gender.ToString();
                cmbTrain.Text = passenger.Train.Id.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show("fatal error: " + ex.Message);
            }

        }


        private void DeletePassenger_ButtonClick(object sender, RoutedEventArgs e)
        {
            //Passenger passenger = lvPassengers.SelectedItem as Passenger;
            if (currentlyEditedPassenger == null) return; // should never happen
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n" + currentlyEditedPassenger, Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.ctx.Passengers.Remove(currentlyEditedPassenger);
                    Globals.ctx.SaveChanges();
                    lvPassengers.ItemsSource = (from f in Globals.ctx.Passengers select f).ToList<Passenger>();
                }
                catch (DataException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
                catch (SystemException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
            }
        }

    }
}
