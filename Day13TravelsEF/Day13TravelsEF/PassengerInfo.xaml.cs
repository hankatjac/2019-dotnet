﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day13TravelsEF
{
    /// <summary>
    /// Interaction logic for PassengerInfo.xaml
    /// </summary>
    public partial class PassengerInfo : Window
    {
        Train currentTrain;
        public PassengerInfo(Window owner, Train train)
        {
          
            InitializeComponent();
            currentTrain = train;
            Owner = owner;
            try
            {
                lvPassengers.ItemsSource = (from p in Globals.ctx.Passengers where p.Train.Id == currentTrain.Id select p).ToList<Passenger>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("fatal error: " + ex.Message);
            }

        }
    }
}
