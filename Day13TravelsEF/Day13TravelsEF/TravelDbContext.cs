﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13TravelsEF
{
    class TravelDbContext : DbContext
    {
        public virtual DbSet<Train> Trains { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }
    }
}
