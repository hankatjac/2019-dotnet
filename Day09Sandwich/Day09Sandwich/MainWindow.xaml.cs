﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09Sandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
 
        }

        private void AddSandwich_MenuClick(object sender, RoutedEventArgs e)
        {

            AddEditDialog dialog = new AddEditDialog(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                //lblBread.Content = AddEditDialog.bread;
                //lblVeggies.Content = AddEditDialog.veggies;
                //lblMeat.Content = AddEditDialog.meat;

                lblBread.Content = AddEditDialog.sandwich.Bread;
                lblVeggies.Content = AddEditDialog.sandwich.Veggies;
                lblMeat.Content = AddEditDialog.sandwich.Meat;
            }
        }

       
    }
}
