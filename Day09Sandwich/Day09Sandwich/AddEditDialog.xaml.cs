﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09Sandwich
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        //public static string bread;
        //public static string veggies;
        //public static string meat;

        public static Sandwich sandwich;
        public Sandwich Sandwich { get; set; }

        MainWindow mainWindow;
        public AddEditDialog(MainWindow owner)
        {
            Owner = owner;
            //mainWindow = owner;
            
            InitializeComponent();
    
        }

        private void Save_ButtonClick(object sender, RoutedEventArgs e)
        {
          string bread = cmbBread.Text;
            List<string> veggiesList = new List<string>();

            if (cbLettuce.IsChecked == true)
            {
                veggiesList.Add("Lettuce");
            }

            if (cbTomatoes.IsChecked == true)
            {
                veggiesList.Add("Tomatoes");
            }

            if (cbCucumbers.IsChecked == true)
            {
                veggiesList.Add("Cucumber");
            }

           string veggies = String.Join(",", veggiesList);


            Meat meat = Meat.chickens;

            if (rbChicken.IsChecked == true)
            {
                meat = Meat.chickens;
            }

            if (rbBeef.IsChecked == true)
            {
                meat = Meat.beef;
            }
            if (rbTurkey.IsChecked == true)
            {
                meat = Meat.turkey;
            }

            // add
           sandwich = new Sandwich() { Bread = bread, Veggies = veggies, Meat = meat };

            //if (rbChicken.IsChecked == true)
            //{
            //    meat = rbChicken.Content.ToString();
            //}

            //if (rbBeef.IsChecked == true)
            //{
            //    meat = rbBeef.Content.ToString();
            //}
            //if (rbTurkey.IsChecked == true)
            //{
            //    meat = rbTurkey.Content.ToString();
            //}


            DialogResult = true; // Close dialog with success result
        }
    }
}
