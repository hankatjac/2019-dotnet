﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09Sandwich
{
   public class Sandwich
    {
        public string Bread {get; set;}
        
        public string Veggies { get; set; }
        public Meat Meat { get; set; }

    }

    public enum Meat { chickens, beef, turkey }
}
