﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09CustDialogCarsDb
{
    public class Database
    {
        private SqlConnection conn;

        // Note: Handle SqlException and SystemException when using constructor
        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Hank\Documents\2019-dotnet\Day09CustDialogCarsDb\CarsDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public List<Car> GetAllCars(string order)
        {
            List<Car> result = new List<Car>();
            SqlCommand command = new SqlCommand("SELECT * FROM Car ORDER BY " + order, conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                // while there is another record present
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string makeModel = (string)reader["MakeModel"];
                    double engineSize = (double)reader["EngineSize"];

                    // FIXME: decided what to do with parsing exception
                    FuelType fuelType = (FuelType)Enum.Parse(typeof(FuelType), (string)reader["FuelType"]);

                    Car car = new Car() { Id = id, MakeModel = makeModel, EngineSize = engineSize, FuelType = fuelType};
                    result.Add(car);
                }
            }
            return result;
        }
        public void AddCar(Car car)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Car (MakeModel, EngineSize, FuelType) VALUES (@MakeModel, @EngineSize, @FuelType)", conn);
            command.Parameters.AddWithValue("@MakeModel", car.MakeModel);
            command.Parameters.AddWithValue("@EngineSize", car.EngineSize);
            command.Parameters.AddWithValue("@FuelType", car.FuelType.ToString());
            
            command.ExecuteNonQuery();
        }

        public void UpdateCar(Car car)
        {
            SqlCommand command = new SqlCommand("UPDATE Car SET MakeModel=@MakeModel,  EngineSize=@EngineSize, FuelType=@FuelType  WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@MakeModel", car.MakeModel);
            command.Parameters.AddWithValue("@EngineSize", car.EngineSize);
            command.Parameters.AddWithValue("@FuelType", car.FuelType.ToString());
            command.Parameters.AddWithValue("@Id", car.Id);
            command.ExecuteNonQuery();
        }

        public void DeleteCar(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Car WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }





    }
}
