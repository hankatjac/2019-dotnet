﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09CustDialogCarsDb
{
    public class Car
    {
        public int Id { get; set; }   
        public string MakeModel { get; set; }
        public double EngineSize { get; set; }
        public FuelType FuelType { get; set; }
        public override string ToString()
        { // TODO: fix date formatting to only display date
            return $"{Id}: {MakeModel} {EngineSize} {FuelType} ";
        }

    }
    public enum FuelType { Gasoline, Diesel, Electric, Hybrid }
}
