﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuizTodoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string sortOrder = "Id";
       
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                lvTodos.ItemsSource = Globals.Db.GetAllTodos(sortOrder);
            }
            catch (SystemException ex) //(SqlException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
                Close(); // Fatal error - exit application
            }
        }

        private void AddTodo_ButtonClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                //lvTodos.Items.Refresh();
                lvTodos.ItemsSource = Globals.Db.GetAllTodos("Id");
            }
            lblStatus.Text = "Add Button is clicked.";

        }

        private void LvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo todo = lvTodos.SelectedItem as Todo;
            if (todo == null) return;
            AddEditDialog dialog = new AddEditDialog(this, todo);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                lvTodos.Items.Refresh();
            }
            lblStatus.Text = "Mouse double is clicked.";
        }

 

        private void Exit_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsCollection = lvTodos.SelectedItems;
            if (selectedItemsCollection.Count == 0)
            { // TODO: make MB nicer
                MessageBox.Show("Select some records first");
            }

            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "CSV file (*.csv)|*.csv|Any file (*.*)|*.*"
            };
            sfd.ShowDialog();
            if (sfd.FileName != "")
            {
                List<string> linesList = new List<string>();
                foreach (var item in selectedItemsCollection)
                {
                    Todo todo = item as Todo;
                    linesList.Add($"{todo.Id};{todo.Task};{todo.Status};{todo.DueDate}");
                }
                try
                {
                    File.WriteAllLines(sfd.FileName, linesList);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing to file:\n" + ex.Message, Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            lblStatus.Text = "File Export menu is clicked.";
        }

            private void Delet_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Todo todo = lvTodos.SelectedItem as Todo;
            if (todo == null) return; // should never happen
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n" + todo, Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeleteTodo(todo.Id);
                    lvTodos.ItemsSource = Globals.Db.GetAllTodos(sortOrder);
                }
                catch (SqlException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
            }
            lblStatus.Text = "Delete menu is clicked.";
        }

        private void SortByTask_ButtonClick(object sender, RoutedEventArgs e)
        {
            List<Todo> todoList = Globals.Db.GetAllTodos("Id");
            todoList = (from t in todoList orderby t.Task select t).ToList<Todo>();
            lvTodos.ItemsSource = todoList;
            lblStatus.Text = "Sort by Task Button is clicked.";
        }

        private void SortByDueDate_ButtonClick(object sender, RoutedEventArgs e)
        {
            List<Todo> todoList = Globals.Db.GetAllTodos("Id");
            todoList = (from t in todoList orderby t.DueDate select t).ToList<Todo>();
            lvTodos.ItemsSource = todoList;
            lblStatus.Text = "Sort by due date Button is clicked.";
        }
    }
}


//SQL script
//CREATE TABLE[dbo].[Todos]
//(

//   [Id] INT           IDENTITY(1, 1) NOT NULL,

//[Task]    NVARCHAR(50) NOT NULL,

//[Status]  NVARCHAR(50) NOT NULL,

//[DueDate] DATETIME NOT NULL,
//    PRIMARY KEY CLUSTERED([Id] ASC),
//    CONSTRAINT[CK_Todos_Status] CHECK([Status] = 'Done'
//                                            OR[Status] = 'Pending')
//);

