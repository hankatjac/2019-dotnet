﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuizTodoList
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Todo currentlyEditedTodo;

        public AddEditDialog(Window owner, Todo todo = null)
        {
            
            InitializeComponent();
            Owner = owner;
            currentlyEditedTodo = todo;
            btAdd.Content = todo == null ? "Add todo" : "Update todo";
            if (todo != null)
            {
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;

                if (todo.Status.ToString() == "Done")
                {
                    cbStatus.IsChecked = true;
                } 
            }
        }

        private void AddUpdateTodo_ButtonClick(object sender, RoutedEventArgs e)
        {
            string task = tbTask.Text;
            DateTime dueDate = dpDueDate.SelectedDate.Value;
            Status status = cbStatus.IsChecked == true ? Status.Done : Status.Pending;
            if (task.Length < 1 || task.Length > 50)
            {
                MessageBox.Show("Task description must be between 1-50 characters long");
                return;
            }
            try
            {
                if (currentlyEditedTodo != null)
                { // update
                    currentlyEditedTodo.Task = task;
                    currentlyEditedTodo.DueDate = dueDate;
                    currentlyEditedTodo.Status = status;

                    Globals.Db.UpdateTodo(currentlyEditedTodo);
                }
                else
                { // add
                    Todo todo = new Todo() { Task = task, DueDate = dueDate, Status = status };
                    Globals.Db.AddTodo(todo);

                }
            }
            catch (SystemException ex) //(SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
            }
            DialogResult = true; // Close dialog with success result
        }

    }
}
