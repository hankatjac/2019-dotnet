﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PrimeIndexer
{


    class IndexedNames
    {
        private string[] namelist = new string[10];
        public string this[int index]
        {
            get
            {
                string tmp;

                if (index >= 0 && index < namelist.Length)
                {
                    tmp = namelist[index];
                }
                else
                {
                    tmp = "";
                }

                return (tmp);
            }
            set
            {
                if (index >= 0 && index < namelist.Length)
                {
                    namelist[index] = value;
                }
            }
        }

    }
    class Program
    {

        static void Main(string[] args)
        {
            IndexedNames names = new IndexedNames();
            names[0] = "Zara";
            names[1] = "Riz";
            names[2] = "Nuha";
            names[3] = "Asif";
            names[4] = "Davinder";
            names[5] = "Sunil";
            names[6] = "Rubic";

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(names[i]);
            }
            Console.ReadKey();

        }
    }
}
