﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Program
    {

        const string DataFileName = @"..\..\people.txt";
        static List<Person> peopleList = new List<Person>();
        static void AddPersonInfo()
        {
            Console.WriteLine("adding a person");
            Console.Write("Enter Name:");
            string name = Console.ReadLine();
            Console.Write("Enter Age:");
            String ageStr = Console.ReadLine();
            if (!int.TryParse(ageStr, out int age))
            {
                Console.WriteLine("age must be a valid integer");
                return;
            }

            Console.Write("Enter City:");
            string city = Console.ReadLine();
            try
            {
                Person p = new Person()
                {
                    Name = name,
                    Age = age,
                    City = city,
                };

                peopleList.Add(p);
                Console.Write("Peson added.");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Person data invalid" + ex.Message);
            }

        }

        static void ListAllPersonsInfo()
        {
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }
        }



        static void FindPersonByName()
        {
            Console.WriteLine("Enter partial name to look for :");
            string name = Console.ReadLine().ToUpper();
            var matchList = from p in peopleList where p.Name.ToUpper().Contains(name) select p;
            Console.WriteLine("Matching person");
            foreach (var m in matchList)
            {
                Console.WriteLine(m);
            }

        }
        static void FindPersonYoungerThan()
        {
            Console.WriteLine("Enter maximum age to look for :");
            string maxAgeStr = Console.ReadLine();
            if (!int.TryParse(maxAgeStr, out int maxAge))
            {
                Console.WriteLine("erro: invalid age");
                return;
            }

            var matchList = from p in peopleList where p.Age <= maxAge select p;
            if (matchList.Count() == 0)
            {
                Console.WriteLine("No matching persons found");
            }
            else
            {
                Console.WriteLine("Matching persons: ");
                foreach (var m in matchList)
                {
                    Console.WriteLine(m);
                }
            }

        }

        static void ReadAllPeopleFromFile()
        {
            peopleList.Clear();
            try
            {
                //if(!File.Exists(DataFileName))
                //{
                //    return;
                //}
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (string line in linesArray)
                {

                    string[] data = line.Split(';');
                    if (data.Length != 3)
                    {
                        Console.WriteLine("Error: invalid number of fileds in line: " + line);
                    }

                    string name = data[0];
                    string ageStr = data[1];
                    // parse the age
                    if (!int.TryParse(ageStr, out int age))
                    {
                        Console.WriteLine("Error: failed to rparse age in line: " + line);
                        continue;
                    }
                    string city = data[2];


                    // create person, add to list

                    try
                    {
                        Person p = new Person()
                        {
                            Name = name,
                            Age = age,
                            City = city,
                        };

                        peopleList.Add(p);
                        Console.Write("Peson added: ");
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Person data invalid" + ex.Message);
                    }

                    Console.WriteLine($"{name} {ageStr} {city}");
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Person reading form file" + ex.Message);
            }
        }

        static void SaveAllPeopleToFile()
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (var p in peopleList)
                {
                    linesList.Add(p.ToDataString());
                }

                File.WriteAllLines(DataFileName, linesList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Person reading form file" + ex.Message);
            }
        }

        static int GetMeunuChoice()
        {
            Console.Write(
            @"What do your want to do?   
            1. Add person info
            2. List persons info
            3. Find a person by name
            4. Find all persons younger than age
            0. Exit
           
            Choice:");

            try
            {
                String choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }


            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("invalid value entered\n");

                }
                else throw ex; // if we don't handle and exception we must throw it!
            }

            return -1;
        }

        static void Main(string[] args)
        {

            ReadAllPeopleFromFile();
            int choice;
            do
            {
                choice = GetMeunuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:// exit
                        break;
                    default:
                        Console.WriteLine("invalid choice try again");
                        break;

                }
                Console.WriteLine();
            } while (choice != 0);

            SaveAllPeopleToFile();
        }

    }
}
