﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Person
    {

        private string _name; // Name 2-100 characters long, not containing semicolons
        private int _age; // Age 0-150
        private string _city; // City 2-100 characters long, not containing semicolons

  
    
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length <= 2 || value.Length >= 100 || value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  2- 100 characters long, cannot contain semicolons");
                }
                _name = value;
            }
        }


        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value <= 0 || value >= 150)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _age = value;
            }
        }


        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length <= 2 || value.Length >= 100 || value.Contains(";"))

                {
                    throw new ArgumentException("city must be 2- 100 characters long, cannot contain semicolons");
                }
                _city = value;
            }
        }

        public override string ToString()
        {
            return $"{Name} is {Age} from {City}";
        }

        // serialiazation data
        public string ToDataString()
        {
            return $"{Name};{Age};{City}";
        }
    }
}