﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04LoggerAnalyzer
{
    class Program
    {
        const string DataFileName = @"..\..\records.txt";
        static List<LogMsg> logList = new List<LogMsg>();
        static void ReadLogsFromFile()
        {
            logList.Clear();

            try
            {
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (string line in linesArray)
                {
                    string[] data = line.Split(';');
                    if (data.Length != 4)
                    {
                        Console.WriteLine("Error: invalid number of fields in line: " + line);
                        continue;
                    }

                    DateTime myDate = DateTime.ParseExact(data[0],"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    string myIndex = data[1];
                    if (!int.TryParse(myIndex, out int n))
                    {
                        Console.WriteLine("Error: failed to parse age in line: " + line);
                        continue;
                    }

                    int myPrime = int.Parse(data[2]);

                    string myMessage = data[3];
                    // create Person, add to list
                    try
                    {
                        LogMsg logMsg = new LogMsg() { Date = myDate, N = n, Prime = myPrime, Msg = myMessage };
                        logList.Add(logMsg);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Error: Person data invalid: " + ex.Message);
                        // continue;
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                // ignore
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading from file " + ex.Message);
            }
        }

        //  You will then use LINQ queries to find out and print out the following:

        //A) All log messages for prime vales below 100, ordered by date.

        static void MsgPrime100() // IMPLEMENT USING LINQ
        {
            Console.WriteLine("All log messages for prime vales below 100, ordered by date. ");
           
            var matchList = from logMsg in logList where logMsg.Prime <100 orderby logMsg.Date select logMsg;
           
            foreach (var m in matchList)
            {
                Console.WriteLine(m);
            }
        }



        //B) All log messages ordered by number of seconds(and seconds only) when they were recorded.

        static void MsgOrderbySeconds() // IMPLEMENT USING LINQ
        {
            Console.WriteLine("All log messages ordered by number of seconds(and seconds only) when they were recorded ");

            var matchList = from logMsg in logList orderby logMsg.Date.Second select logMsg;

            foreach (var m in matchList)
            {
                Console.WriteLine(m);
            }
        }

        static void Main()
        {
            ReadLogsFromFile();
            MsgPrime100();
            MsgOrderbySeconds();

            Console.ReadKey();
        }
    }
}
