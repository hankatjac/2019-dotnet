﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04LoggerAnalyzer
{
    class LogMsg
    {
        public DateTime Date { get; set; }
        public int N { get; set; }
        public long Prime { get; set; }
        public string Msg { get; set; }

        public override string ToString()
        {
            return $"{Date} {N} {Prime} {Msg}";
        }

     



    }
}
