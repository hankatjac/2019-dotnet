﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04JustNumbers
{
    class Program
    {

        static List<int> intList = new List<int>();
       


        static void Main(string[] args)
        {
            const string DataFileName = @"..\..\output.txt";
            try
            {
                while (true)
                {
                    Console.Write("Enter a positve number: ");
                    int num = int.Parse(Console.ReadLine());
                    if (num == 0)
                    {
                        break;
                    }
                    if (num <= 0)
                    {
                       throw new ArgumentException("number must be positive number");
                    }
                    intList.Add(num);
                }

                //
               

                //
                // var foundList = from n in namesList where n.ToUpper().Contains(search) select n;
                double avg = intList.Average();

                Console.WriteLine($"average:{avg:0.00}");


                
                Console.WriteLine($"Max number:{intList.Max()}");

                double median;
                if (intList.Count() % 2 == 0)
                { // even
                    double v1 = intList[intList.Count() / 2 - 1];
                    double v2 = intList[intList.Count() / 2];
                    median = (v1 + v2) / 2;
                }
                else
                { // odd
                    median = intList[intList.Count() / 2];
                }
                Console.WriteLine($"Median is: {median:0.00}");

                // compute stdandard deviation

                double sumOfSquares = 0;
                foreach (double n in intList)
                {
                    sumOfSquares += Math.Pow((n - avg), 2);

                }
                double stddev = Math.Sqrt(sumOfSquares / intList.Count());
                Console.WriteLine($"Standard deviation is {stddev:0.00}");

                try
                {
                    List<string> linesList = new List<string>();
                    foreach (var num in intList)
                    {
                        linesList.Add(num.ToString());
                    }

                    File.WriteAllLines(DataFileName, linesList);
                }
                catch (IOException ex)
                {
                    Console.WriteLine("Error writing file " + ex.Message);
                }
            
            }
            finally
            {
                Console.WriteLine("Press a key to finish");
                Console.ReadKey();
            }
        }
    }
}
