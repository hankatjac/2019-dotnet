﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isModified;
        //string path = Directory.GetCurrentDirectory();
        string currOpenFile = "";



        public MainWindow()
        {
            InitializeComponent();
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileNew_MenuClick(object sender, RoutedEventArgs e)
        {

            if (!isModified)
            {
                tbEditor.Text = "";
            }

            Window_Closing(sender, null);


            //if (isModified)
            //{
            //    MessageBoxResult result = MessageBox.Show("Do you want to save changes to the current file?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
            //    if (result == MessageBoxResult.Yes)
            //    {
            //        if (!File.Exists(path))
            //            FileSave_MenuClick(sender, e);
            //        else
            //            FileSaveAs_MenuClick(sender, e);
            //    }
            //    else if (result == MessageBoxResult.Cancel)
            //    {
            //        //code for Cancel

            //        return;
            //    }
            //    tbEditor.Text = string.Empty;
            //}

            //tbEditor.Text = string.Empty;
        }

        private void FileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog
                {
                    Filter = "Text Document (*.txt)|*.txt|All files (*.*)|*.*",
                    Title = "Open",
                    InitialDirectory = @"C:\",
                    RestoreDirectory = false
                };

                if (openFileDialog.ShowDialog() == true)
                {
                    tbEditor.Text = File.ReadAllText(openFileDialog.FileName);
                    sbStatus.Text = openFileDialog.FileName;
                }
               
            }
            catch(IOException ex)
            {
                MessageBox.Show("Erro reading file:\n" +ex.Message, "SharpNotedpad", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileSave_MenuClick(object sender, RoutedEventArgs e)
        {
            //todo: handle the special case when empty file is not modified and  file->save isused

            if (currOpenFile != "")
            {
                File.WriteAllText(currOpenFile, tbEditor.Text);
            }
            else
            {
                FileSaveAs_MenuClick(sender, e);
            }
        }

        private void FileSaveAs_MenuClick(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    // Available file extensions
                    Filter = "Text Document (*.txt)|*.txt|All files (*.*)|*.*",
                    Title = "Save As",
                    // Startup directory
                    InitialDirectory = @"C:\",
                    // Restores the selected directory, next time
                    RestoreDirectory = false
                };

                if (saveFileDialog.ShowDialog() == true)
                {
                    File.WriteAllText(saveFileDialog.FileName, tbEditor.Text);
                    isModified = false;
                    currOpenFile = saveFileDialog.FileName;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error writing to file:\n" + ex.Message,
                    "SharpNotepad", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(!isModified)
            {
                return;
            }

            MessageBoxResult result = MessageBox.Show("Do you want to save changes to the current file?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
            switch (result)
            {
              
                case MessageBoxResult.Yes:
                    FileSave_MenuClick(sender, null);
                    // todo: if file was saved then we should tbeditor.txt ="";
                    if (!isModified && e == null)
                    {
                        tbEditor.Text = "";
                    }
                    break;
                case MessageBoxResult.Cancel:
                    if (e != null)// prevent window from closing
                    {
                        e.Cancel = true;
                    }
                    break;
               
                case MessageBoxResult.No:
                    //if (e == null)
                    {
                        tbEditor.Text = "";
                        isModified = false;
                    }
                    break;
                default:
                    MessageBox.Show("Internal error - unknown choice");
                    break;
            }
        }

        private void TbEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            isModified = true;
        }
            
    }
    
}
