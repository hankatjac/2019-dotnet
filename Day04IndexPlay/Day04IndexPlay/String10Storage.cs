﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04IndexPlay
{
    class String10Storage<T>
    {
        private T[] string10Storage = new T[10];
        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= string10Storage.Length)
                    throw new IndexOutOfRangeException("Cannot store more than 10 objects");

                return string10Storage[index];
            }
            set
            {
                if (index < 0 || index >= string10Storage.Length)
                    throw new IndexOutOfRangeException("Cannot store more than 10 objects");

                string10Storage[index] = value;
            }

        }
    }
}
