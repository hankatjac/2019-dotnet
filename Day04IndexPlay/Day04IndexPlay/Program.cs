﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04IndexPlay
{
    class Program

    { 
        static void DisplayMessage(int n, string msg)
        {
            int a = 0, b = 1;
            int c;

            PrimeArray1 pa = new PrimeArray1();
            for (int i = 0; i < n; i++)
            {
                c = a + b;
                a = b;
                b = c;

                    Console.WriteLine($"{c,2} {pa[c],10} {msg}");

            }
        
        }
        static void SaveMessage(int n, string msg)
        {
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int a = 0, b = 1;
            int c;

            PrimeArray1 pa = new PrimeArray1();
            for (int i = 1; i <= n; i++)
            {
                c = a + b;
                a = b;
                b = c;

                File.AppendAllText(@"..\..\records.txt", $"{now}: {c,2} {pa[c],10} {msg}{Environment.NewLine}");
            }
           
        }

    
        static void Main()
        {
            String10Storage<string> strStore = new String10Storage<string>();


            strStore[0] = "One";
            strStore[1] = "Two";
            strStore[2] = "Three";
            strStore[3] = "Four";

            strStore[7] = "Seven";

            for (int i = 0; i < 10; i++)
                Console.WriteLine(strStore[i]);

            PrimeArray boolStr = new PrimeArray();

            for (int i = 1; i < 10; i++)
                Console.WriteLine($"{i,2}: { boolStr[i],10}");

            PrimeArray1 pa = new PrimeArray1();

            for (int i = 1; i < 10; i++)
                Console.WriteLine($"{i,2}: { pa[i],10}");

            Random rand = new Random();
            
            try
            {
                pa.logger = null;
                if (rand.Next(0, 2) == 0)
                {
                    pa.logger = DisplayMessage;
                }
                    
                else
                {
                    pa.logger = SaveMessage;
                }



                pa.logger?.Invoke(8, "computed new number");
            }
            finally
            {

                Console.ReadKey();
            }


        }
    }
}
