﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04IndexPlay
{
    class PrimeArray
    {
        private  bool[] primeindex = new bool[10];
        public bool this[int index]
        {
            get
            {
                if (index <= 0 || index >= primeindex.Length)
                    throw new IndexOutOfRangeException("Cannot store more than 10 objects");

                primeindex[1] = true;
                
               
                for (int i = 2; i < 10; i++)
                {
                    primeindex[i] = true;
                    for (int j = 2; j <= i / 2; j++)
                    {
                        if (i % j == 0)
                        {
                            primeindex[i] = false;
                            break;
                        }
                    }
                }



                return primeindex[index];
            }
            

        }
    }
}
