﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07FriendsList
{
    class Friend
    {
        public string Name;
        public int Age;

        public override string ToString()
        {
            return $"{Name} is {Age} y/o";
        }

        public static bool IsNameValid(string name)
        {
            return (name.Length < 1 || name.Length > 50 || name.Contains(";"));
           
        }

        public static bool IsAgeValid(int age)
        {
            return (age>=1 && age <=150);

        }

        public static bool IsAgeValid(string ageStr)
        {
            bool result = int.TryParse(ageStr, out int age);
            if (!result) return false;
            return (age >= 1 && age <= 150);
        }


    }
}
