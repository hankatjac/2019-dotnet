﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06Convertor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
                //int cNum = int.Parse(tbConvert.Text);
                //int fNum = (cNum * 9 / 5) + 32;
                //lblConvert.Content = fNum;

                string celStr = tbConvert.Text;
                if (!double.TryParse(celStr, out double cel))
                {
                    MessageBox.Show("Value must be numerical", "tempconv", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                double fah = 9.0 / 5 * cel + 32;
                lblConvert.Content = $"{ fah: 0.000}F";
 
            
        }
               
            
           
        
    }
}
