﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain
{
    class Person
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length <= 1 || value.Length >= 50 || value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  2- 100 characters long, cannot contain semicolons");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value <= 0 || value >= 150)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _age = value;
            }
        }

        protected Person()
        { }

        public Person(string name, int age)
        {
            Name= name;
            Age = age;
        }

        public Person(string dataline)
        {
            string[] data = dataline.Split(';');
            if (data.Length!=3)
            {
                throw new InvalidParameterException("line for person must have exactly 3 values");
            }
            if( data[0] != "Person")
            {
                //this check is redundant and we still want to have it
                throw new InvalidParameterException("line must be person type");
            }
            Name = data[1];
            string ageStr = data[2];

            try
            {
                Age = int.Parse(ageStr);
            }
            catch(Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {//exception chaining (translate one exception into another)
                    throw new InvalidParameterException("Integer value expeted", ex);
                }
                else throw ex;

            }
           
        }

        public override string ToString()
        {
            return $"Peson: {Name} is {Age} ";
        }

        // serialiazation data
        public virtual string ToDataString()
        {
            return $"Person: {Name};{Age}";
        }

    }
}
