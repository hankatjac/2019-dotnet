﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain
{
    class Teacher:Person
    {
        private string _subject; // 1-50 characters, no semicolons
        private int _yoe; // 0-100


 

        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length <= 1 || value.Length >= 50|| value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  1- 50 characters long, cannot contain semicolons");
                }
                _subject = value;
            }
        }

        public int YearsOfExperience
        {
            get
            {
                return _yoe;
            }
            set
            {
                if (value <= 0 || value >= 100)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _yoe = value;
            }
        }



         public Teacher(string name, int age, string subject, int yearOfExperience) : base(name, age)
        {
            Subject= subject;
            YearsOfExperience = yearOfExperience;
        }


       public Teacher (string dataline):base()
        {
            string[] data = dataline.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterException("line for teacher must have exactly 5 values");
            }
            if (data[0] != "Teacher")
            {
                //this check is redundant and we still want to have it
                throw new InvalidParameterException("Line must be teacher type");
            }
            Name = data[1];
            string ageStr = data[2];

            try
            {
                Name = data[1];
                
                Age = int.Parse(ageStr);

                Subject = data[3];
                YearsOfExperience = int.Parse(data[4]);
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {//exception chaining (translate one exception into another)
                    throw new InvalidParameterException("Integer value expeted", ex);
                }
                else throw ex;

            }


        }

        public override string ToString()
        {
            return $"Teacher {Name} is {Age} teaching {Subject} with year of experience {YearsOfExperience}";
        }


        public override string ToDataString()
        {
            return $"Teacher: {Name};{Age};{Subject};{YearsOfExperience}";
        }





    }
}
