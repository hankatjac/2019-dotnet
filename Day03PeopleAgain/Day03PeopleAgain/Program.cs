﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain
{

    public class InvalidParameterException: Exception
    {
        public InvalidParameterException() { }
        public InvalidParameterException(string msg) : base(msg) { }
        public InvalidParameterException(string msg, Exception orig) : base(msg, orig) { }


    }



    class Program
    {
        static List<Person> peopleList = new List<Person>();

        static void ProcessingTask()
        {
            // display all as is
            Console.WriteLine("\nAll items:");
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }

            // display only student type
            Console.WriteLine("\nOnly Students items:");
            foreach (Person p in peopleList)
            {
                if (p is Student)
                {
                    Console.WriteLine(p);
                }
            }

            // display only teacher type
            Console.WriteLine("\nOnly Teachers items:");
            foreach (Person p in peopleList)
            {
                if (p is Teacher)
                {
                    Console.WriteLine(p);
                }
            }

            // display only person type
            Console.WriteLine("\nOnly Person items:");
            foreach (Person p in peopleList)
            {
                if (p.GetType().Equals(typeof(Person)))
                {
                    Console.WriteLine(p);
                }
            }

            //Student s3 = p as Student; if cast is not possible then s3 is null
        }



        static void ProcessingAverageTask()
        {// using linq
            var studentsCollection = from p in peopleList where p is Student select p as Student;
            if (studentsCollection.Count() == 0)
            {
                Console.WriteLine("no students found to compute averages on");
            }
            double avg = studentsCollection.Average(s => s.GPA);
            Console.WriteLine($"Average GPA is :{avg:0.00}");

            //compute median
            double medianGPA;
            List<double> GPASortedList= (from s in studentsCollection orderby s.GPA select s.GPA).ToList<double>();
            if (GPASortedList.Count() %2 == 0)
            {
                double v1 = GPASortedList[GPASortedList.Count() / 2 - 1];
                double v2 = GPASortedList[GPASortedList.Count() / 2];
                medianGPA = (v1 + v2) / 2;

            }
            else
            {
                medianGPA = GPASortedList[GPASortedList.Count() / 2];
            }

            Console.WriteLine($"Median is : {medianGPA:0.00}");


            // compute standard deviation
            double sumOfSquares = 0;
            foreach (double gpa in GPASortedList)
            {
                sumOfSquares += (gpa - avg) * (gpa - avg);
            }

            double stdDev = Math.Sqrt(sumOfSquares / GPASortedList.Count());
            Console.WriteLine($"stand deviation  is : {stdDev:0.00}");
        }



        static void Main()
        {

            try
            {// open  file and parse items, add to list
                string[] linesArray = File.ReadAllLines(@"..\..\people.txt");
                foreach (string line in linesArray)
                {
                    try
                    {
                        string typeName = line.Split(';')[0];
                        switch (typeName)
                        {
                            case "Person":
                                Person p = new Person(line);
                                peopleList.Add(p);
                                break;

                            case "Teacher":
                                Teacher t = new Teacher(line);
                                peopleList.Add(t);
                                break;

                            case "Student":
                                Student s = new Student(line);
                                peopleList.Add(s);
                                break;

                            default:
                                Console.WriteLine("Error in data line: don't know how to make " + typeName);
                                break;
                        }
                    }
                    catch (InvalidParameterException ex)
                    {
                        Console.WriteLine("Error parsing line: " + ex.Message);
                    }
                }
                // display all
                foreach (Person p in peopleList)
                {
                    Console.WriteLine(p);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            } finally
            {
                Console.WriteLine("Press any key to finish");    
            }

            ProcessingTask();
            ProcessingAverageTask();

            Console.ReadKey();
        }
    }
}
