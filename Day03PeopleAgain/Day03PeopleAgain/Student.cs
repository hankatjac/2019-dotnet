﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03PeopleAgain
{
    class Student:Person
    {


        private string _program; // 1-50 characters, no semicolons
        private double _gpa; // 0-4.3


        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length <= 2 || value.Length >= 100 || value.Contains(";"))
                {
                    throw new ArgumentException("name  must be  2- 100 characters long, cannot contain semicolons");
                }
                _program = value;
            }
        }

        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value <= 0 || value >= 4.3)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(Age)} must be > 0 and < 150");
                }

                _gpa = value;
            }
        }

        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            Program = program;
            GPA = gpa;
        }

        public Student(string dataline) : base()
        {
            string[] data = dataline.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterException("line for student must have exactly 5 values");
            }
            if (data[0] != "Student")
            {
                //this check is redundant and we still want to have it
                throw new InvalidParameterException("line must be student type");
            }
            Name = data[1];
            string ageStr = data[2];

            try
            {
                Name = data[1];

                Age = int.Parse(ageStr);

                Program = data[3];
                GPA = double.Parse(data[4]);
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {//exception chaining (translate one exception into another)
                    throw new InvalidParameterException("Integer value expeted", ex);
                }
                else throw ex;

            }

        }
        public override string ToString()
        {
            return $"Student {Name} is {Age} studying {Program} with GPA {GPA:0.00}";
        }

        public override string ToDataString()
        {
            return $"Studnent: {Name};{Age};{Program};{GPA}";
        }


    }
}
