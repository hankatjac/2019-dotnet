﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day09Passenger
{
    public class Passenger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Passport { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureDate { get; set; }

        public override string ToString()
        {
            return $"Passenger: {Name} {Passport} to {Destination} on {DepartureDate.ToString("MMMM dd, yyyy HH:mm")}";
        }

        public static bool IsDestinationValid(string destination)
        {
            return destination.Length > 1 && destination.Length < 30;
        }

        public static bool IsNameValid(string name)
        {
            return name.Length > 1 && name.Length < 30 || !name.Contains(";");
        }

        public static bool IsPassportValid(string passport)
        {
            return Regex.IsMatch(passport, @"^[A-Z][A-Z][]0-9]{6}$");
        }

    }
}
