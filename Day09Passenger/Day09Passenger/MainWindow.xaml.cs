﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09Passenger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Id");
            }
            catch (SystemException ex) //(SqlException ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
                Close(); // Fatal error - exit application
            }
        }

        private void AddPassenger_ButtonClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
              //lvPassengers.Items.Refresh();
                lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Id");
            }
        }

        private void LvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger passenger = lvPassengers.SelectedItem as Passenger;
            if (passenger == null) return;
            AddEditDialog dialog = new AddEditDialog(this, passenger);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                lvPassengers.Items.Refresh();
            }
        }

        private void Edit_MenuItemClick(object sender, RoutedEventArgs e)
        {
            LvPassengers_MouseDoubleClick(sender, null);
        }


        private void Delete_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Passenger passenger = lvPassengers.SelectedItem as Passenger;
            if (passenger == null) return; // should never happen
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n" + passenger, Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeletePassenger(passenger.Id);
                    lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Id");
                }
                catch (SqlException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
            }
        }

        private void Sort_ButtonClick(object sender, RoutedEventArgs e)
        {
            SortDialog dialog = new SortDialog(this);
            dialog.ShowDialog();

        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lvPassengers.ItemsSource = Globals.Db.SearchName(tbSearch.Text).Tables[0].DefaultView;
        }

        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {

            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    var columnBinding = headerClicked.Column.DisplayMemberBinding as Binding;
                    var sortBy = columnBinding?.Path.Path ?? headerClicked.Column.Header as string;

                    Sort(sortBy, direction);

                    if (direction == ListSortDirection.Ascending)
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowUp"] as DataTemplate;
                    }
                    else
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowDown"] as DataTemplate;
                    }

                    // Remove arrow from previously sorted header
                    if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                    {
                        _lastHeaderClicked.Column.HeaderTemplate = null;
                    }

                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }

        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(lvPassengers.ItemsSource);

            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }
    }
}



