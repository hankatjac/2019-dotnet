﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09Passenger
{
    /// <summary>
    /// Interaction logic for SortDialog.xaml
    /// </summary>
    public partial class SortDialog : Window
    {
        MainWindow mw;
        public SortDialog(MainWindow owner)
        {
            InitializeComponent();
            mw = owner;
            Owner = owner;
        }

        private void Apply_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (rbName.IsChecked ==true)
            {
                mw.lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Name");
            }
            if (rbPassport.IsChecked == true)
            {
                mw.lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Passport");
            }
            if (rbDestination.IsChecked == true)
            {       
                mw.lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Destination");
            }
            if (rbDepartureDate.IsChecked == true)
            {   
                mw.lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("DepartureDate");
            }

            DialogResult = true; // Close dialog with success result
        }
    }
}
