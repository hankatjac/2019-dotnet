﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09Passenger
{

    public partial class AddEditDialog : Window
    {
        MainWindow mw;
        Passenger currentlyEditedPassenger;
        public AddEditDialog(MainWindow owner, Passenger passenger = null)
        {
            InitializeComponent();
            mw = owner;
            Owner = owner;
            for (int hour = 0; hour < 24; hour++)
            {
                cmbDepartureTime.Items.Add($"{hour:00}:00");
                cmbDepartureTime.Items.Add($"{hour:00}:30");
            }
            cmbDepartureTime.SelectedValue = "00:00";

            currentlyEditedPassenger = passenger;
            if (passenger != null)
            {
                lblId.Content = passenger.Id;
                tbName.Text = passenger.Name;
                tbPassport.Text = passenger.Passport;
                tbDestination.Text = passenger.Destination;
                //dpDeparture.Text = passenger.DepartureDate.ToString("MMMM dd, yyyy");
                dpDepartureDate.SelectedDate = passenger.DepartureDate;
                cmbDepartureTime.Text = passenger.DepartureDate.ToString("HH:mm");
            }
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {

            string name = tbName.Text;
            string passport = tbPassport.Text;
            string destination = tbDestination.Text;
            DateTime departureDate = DateTime.Parse(dpDepartureDate.Text + " " + cmbDepartureTime.Text);

            if (currentlyEditedPassenger != null)
            { // update
                currentlyEditedPassenger.Name = name;
                currentlyEditedPassenger.Passport = passport;
                currentlyEditedPassenger.Destination = destination;
                currentlyEditedPassenger.DepartureDate = departureDate;

                Globals.Db.UpdatePassenger(currentlyEditedPassenger);
            }
            else
            { // add
                Passenger passenger = new Passenger() { Name = name, Passport = passport, Destination = destination, DepartureDate = departureDate };
                Globals.Db.AddPassenger(passenger);
            }
            DialogResult = true; // Close dialog with success result
        }

        private void Delete_ButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n", Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    int Id = int.Parse(lblId.Content.ToString());
                    Globals.Db.DeletePassenger(Id);

                }
                catch (SqlException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
                DialogResult = true;
                mw.lvPassengers.ItemsSource = Globals.Db.GetAllPassengers("Id");
            }
        }
    }
}
