﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09Passenger
{
    public class Database
    {
        private SqlConnection conn;

        // Note: Handle SqlException and SystemException when using constructor
        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Hank\Documents\2019-dotnet\Day09Passenger\PassengerDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public List<Passenger> GetAllPassengers(string order)
        {
            List<Passenger> result = new List<Passenger>();

            SqlCommand command = new SqlCommand("SELECT * FROM Passenger ORDER BY " + order, conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                // while there is another record present
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    string passport = (string)reader["Passport"];
                    string destination = (string)reader["Destination"];
                    DateTime departureDate = (DateTime)reader["DepartureDate"];

                    // FIXME: decided what to do with parsing exception

                    Passenger passenger = new Passenger() { Id = id, Name = name, Passport = passport, Destination = destination, DepartureDate = departureDate };
                    result.Add(passenger);
                }
            }
            return result;
        }

        public void AddPassenger(Passenger passenger)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Passenger (Name, Passport, Destination, DepartureDate) VALUES (@Name, @Passport, @Destination, @DepartureDate)", conn);
            command.Parameters.AddWithValue("@Name", passenger.Name);
            command.Parameters.AddWithValue("@Passport", passenger.Passport);
            command.Parameters.AddWithValue("@Destination", passenger.Destination);
            command.Parameters.AddWithValue("@DepartureDate", passenger.DepartureDate);
            command.ExecuteNonQuery();
        }

        public void UpdatePassenger(Passenger passenger)
        {
            SqlCommand command = new SqlCommand("UPDATE Passenger SET Name=@Name, Passport=@Passport, Destination=@Destination, DepartureDate=@DepartureDate WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@Name", passenger.Name);
            command.Parameters.AddWithValue("@Passport", passenger.Passport);
            command.Parameters.AddWithValue("@Destination", passenger.Destination);
            command.Parameters.AddWithValue("@DepartureDate", passenger.DepartureDate);
            command.Parameters.AddWithValue("@Id", passenger.Id);
            command.ExecuteNonQuery();
        }

        public void DeletePassenger(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Passenger WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

        public DataSet SearchName(string name)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter("SELECT * from passenger WHERE (Name like '%" + name + "%')", conn);
            SDA.Fill(ds);
            return ds;
        }

    }
}
