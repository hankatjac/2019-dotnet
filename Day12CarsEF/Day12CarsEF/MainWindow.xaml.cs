﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12CarsEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string sortOrder = "Id";

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new CarDbContext();
                lvCars.ItemsSource = (from c in Globals.ctx.Cars select c).ToList<Car>();
            }
            
            catch (Exception ex)
            { // TODO: make message box nicer
                MessageBox.Show("Database error:\n" + ex.Message);
                Close(); // Fatal error - exit application
            }
        }

        private void AddCar_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                //lvCars.Items.Refresh();
                lvCars.ItemsSource = (from c in Globals.ctx.Cars select c).ToList<Car>(); ;
            }
        }

        private void LvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Car car = lvCars.SelectedItem as Car;
            if (car == null) return;
            AddEditDialog dialog = new AddEditDialog(this, car);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                lvCars.Items.Refresh();
            }
        }

        private void Edit_MenuItemClick(object sender, RoutedEventArgs e)
        {
            LvCars_MouseDoubleClick(sender, null);
        }

        private void Exit_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsCollection = lvCars.SelectedItems;
            if (selectedItemsCollection.Count == 0)
            { // TODO: make MB nicer
                MessageBox.Show("Select some records first");
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text file (*.txt)|*.txt|Any file (*.*)|*.*";
            sfd.ShowDialog();
            if (sfd.FileName != "")
            {
                List<string> linesList = new List<string>();
                foreach (var item in selectedItemsCollection)
                {
                    Car car = item as Car;
                    linesList.Add($"{car.Id};{car.MakeModel};{car.EngineSize};{car.FuelType}");
                }
                try
                {
                    File.WriteAllLines(sfd.FileName, linesList);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file:\n" + ex.Message);
                }
            }
        }

        private void Delet_MenuItemClick(object sender, RoutedEventArgs e)
        {
            Car car = lvCars.SelectedItem as Car;
            if (car == null) return; // should never happen
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this record?\n" + car, Globals.AppName, MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.ctx.Cars.Remove(car);
                    Globals.ctx.SaveChanges();
                    lvCars.ItemsSource = (from c in Globals.ctx.Cars select c).ToList<Car>();
                }
                catch (DataException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
                catch (SystemException ex)
                { // TODO: make message box nicer
                    MessageBox.Show("Database error:\n" + ex.Message);
                }
            }
        }
    }
}
