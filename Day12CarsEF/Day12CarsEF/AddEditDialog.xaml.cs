﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day12CarsEF
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Car currentlyEditedCar;

        public AddEditDialog(Window owner, Car car = null)
        {
            Owner = owner;
            currentlyEditedCar = car;
            InitializeComponent();
            btSaveUpdate.Content = car == null ? "Add car" : "Update car";
            if (car != null)
            {
                tbMakeModel.Text = car.MakeModel;
                slEngineSize.Value = car.EngineSize;
                cmbFuelType.Text = car.FuelType.ToString();
            }
        }

        private void AddUpdateCar_ButtonClick(object sender, RoutedEventArgs e)
        {
            string makeModel = tbMakeModel.Text;
            double engineSize = slEngineSize.Value;
            FuelType fuelType = (FuelType)Enum.Parse(typeof(FuelType), cmbFuelType.Text);

            if (currentlyEditedCar != null)
            { // update
                currentlyEditedCar.MakeModel = makeModel;
                currentlyEditedCar.EngineSize = engineSize;
                currentlyEditedCar.FuelType = fuelType;
                Globals.ctx.SaveChanges();
            }
            else
            { // add
                Car car = new Car() { MakeModel = makeModel, EngineSize = engineSize, FuelType = fuelType };
                Globals.ctx.Cars.Add(car);
                Globals.ctx.SaveChanges();
            }
            DialogResult = true; // Close dialog with success result
        }
    }
}
