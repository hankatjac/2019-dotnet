﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09CustomeDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        public MainWindow()
        {

            InitializeComponent();
            Globals.friendsList.Add(new Friend(){ Name = "Jerry", Age=33});
            Globals.friendsList.Add(new Friend() { Name = "Maria", Age = 44 });
            Globals.friendsList.Add(new Friend() { Name = "Timothy", Age = 21 });
            lvFriend.ItemsSource = Globals.friendsList;
        }


        private void AddFriend_MenuItemClick(object sender, RoutedEventArgs e)
        {
            //Friend friend = null;
            //AddEditDialog dialog = new AddEditDialog(this, friend);
            AddEditDialog dialog = new AddEditDialog(this);

            if (dialog.ShowDialog()==true)
            {//only refresh if data changed
                lvFriend.Items.Refresh();
            }
        }

        private void LvFriend_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Friend friend = lvFriend.SelectedItem as Friend;
            if (friend == null) return;
            AddEditDialog dialog = new AddEditDialog(this, friend);
            if (dialog.ShowDialog() == true)
            {//only refresh if data changed
                lvFriend.Items.Refresh();
            }
        }
    }
}
