﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day12FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            try
            {
                SocietyDBContext ctx = new SocietyDBContext();

                //add record to database
                Person p1 = new Person{ Name = "Maria", Age = random.Next(100) };
                ctx.People.Add(p1); // this not insert
                ctx.SaveChanges();
                Console.WriteLine("record added");

                //update - fetch then update
                var p2 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {
                    p2.Name = "Alibaba";
                    ctx.SaveChanges();
                    Console.WriteLine("Record update");
                }
                else
                {
                    Console.WriteLine("Record to update not found");
                }

                // delete - fetch then delete
                var p3 = (from p in ctx.People where p.Id == 2 select p).FirstOrDefault<Person>();
                if (p3 != null)
                {
                    ctx.People.Remove(p3); // schedule for deletion from database
                    ctx.SaveChanges();
                    Console.WriteLine("Record deleted");
                }
                else
                {
                    Console.WriteLine("Record to delete not found");
                }

                //print all records
                var peopleCol = from p in ctx.People select p;
                foreach (Person p in peopleCol)
                {
                    Console.WriteLine($"{p.Id}:{p.Name} is {p.Age} y/o");
                }

            }
            finally
            {
                Console.WriteLine("Press ay key to finish");
                Console.ReadKey();
            }
        }
    }
}
