﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13CarsAndOwnersEF
{
    public class Owner
    {
        public int Id { get; set; }

        [Required] //means not null
        [StringLength(100)] //nvachar(50)
        public string Name { get; set; } // up to 100 characters
        public virtual ICollection<Car> CarsCollection { get; set; } // relation one owner may have many cars.

        public override string ToString()
        {
            return $"Owner{Id}: {Name}";
        }
    }
}
