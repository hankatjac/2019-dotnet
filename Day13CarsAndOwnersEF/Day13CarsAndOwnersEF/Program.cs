﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13CarsAndOwnersEF
{
    class Program
    {
        static ParkingDbContex ctx;
        private static void ListAllCarsAndTheirOwners()
        {
            var carsList = from car in ctx.Cars select car;
            foreach (Car car in carsList)
            {
                //Console.WriteLine($"{car.Id} {car.MakeModel} {car.YearOfProd} {car.Owner}");
                Console.WriteLine(car);
            }
        }
        private static void ListAllOwnersAndTheirCars()
        {
            var ownersList = from o in ctx.Owners select o;
            foreach (Owner o in ownersList)
            {
                //Console.WriteLine($"{o.Id} {o.Name} {o.CarsCollection} ");
                Console.WriteLine(o);
                if (o.CarsCollection != null)
                {
                    foreach (Car car in o.CarsCollection)
                    {
                        //Console.WriteLine($"{car.Id} {car.MakeModel} {car.YearOfProd} {car.Owner}");
                        Console.WriteLine(car);
                    }
                }
            }
        }

        private static void AddAnOwner_NoCars()
        {
            Console.WriteLine("Enter Owner Name:");
            string name = Console.ReadLine();
            Owner owner = new Owner { Name = name };
            ctx.Owners.Add(owner);
            ctx.SaveChanges();
        }
        private static void AddACar_NoOwner()
        {
            Console.WriteLine("Enter make model:");
            string makeModel = Console.ReadLine();
            Console.WriteLine("Enter year of production:");
            int yearOfProd = int.Parse(Console.ReadLine());
            Car car = new Car { MakeModel = makeModel, YearOfProd = yearOfProd };
            ctx.Cars.Add(car);
            ctx.SaveChanges();
        }

        private static void AssignCartoAnOwnerOrNoOwner()
        {
            // show all the cars with their Ids
            ListAllCarsAndTheirOwners();
            Console.WriteLine("Enter Id of car:");
            int carId = int.Parse(Console.ReadLine());
            var car = (from c in ctx.Cars where c.Id == carId select c).FirstOrDefault<Car>();
            //Car car = ctx.Cars.Find(carId);
            if (car == null)
            {
                Console.WriteLine("Car not found");
                return;
            }
            // show all owners, choose an owenr, empty means set owener to null
            ListAllOwnersAndTheirCars();
            Console.Write("enter id of an owner, -1 to set null");
            int ownerId = int.Parse(Console.ReadLine());
            Owner owner = null;
            if (ownerId != -1)
            {
                owner = ctx.Owners.Find(ownerId);
                if (car == null)
                {
                    Console.WriteLine("Owner not found");
                    return;
                }
            }
            car.Owner = owner;
            ctx.SaveChanges();
            Console.WriteLine("owner changed saved");
        }

        private static void DeleteAnOwnerWithAllCarsTheyOwn()
        {
            ListAllOwnersAndTheirCars();
            Console.WriteLine("Enter Owner Id:");
            int id = int.Parse(Console.ReadLine());
            var owner = (from o in ctx.Owners where o.Id == id select o).FirstOrDefault<Owner>();
            if (owner != null)
            {
                foreach (Car car in owner.CarsCollection.ToList())
                {
                    ctx.Cars.Remove(car);
                }
                ctx.Owners.Remove(owner); // schedule for deletion from database
                Console.WriteLine("Record deleted");
                ctx.SaveChanges();
            }
            else
            {
                Console.WriteLine("Record to delete not found");
            }
        }

        static int GetMeunuChoice()
        {
            Console.Write(
            @" Please choose one of the menu
            1. List all cars and their owner
            2. List all owners and their cars
            3. Add a car (no owner)
            4. Add an owner (no cars)
            5. Assign car to an owner (or no owner)
            6. Delete an owner with all cars they own
            0. Quit
           
            Choice:");

            try
            {
                String choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }


            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("invalid value entered\n");

                }
                else throw ex; // if we don't handle and exception we must throw it!
            }

            return -1;
        }

        static void Main(string[] args)
        {
            ctx = new ParkingDbContex();
            Owner owner1 = new Owner { Name = "Maria", CarsCollection = null };
            int choice;
            do
            {
                choice = GetMeunuChoice();
                switch (choice)
                {
                    case 1:
                        ListAllCarsAndTheirOwners();

                        break;
                    case 2:
                        ListAllOwnersAndTheirCars();
                        break;
                    case 3:
                        AddACar_NoOwner();

                        break;
                    case 4:

                        AddAnOwner_NoCars();

                        break;
                    case 5:
                        AssignCartoAnOwnerOrNoOwner();
                        break;
                    case 6:
                        DeleteAnOwnerWithAllCarsTheyOwn();
                        break;

                    case 0:// exit
                        break;
                    default:
                        Console.WriteLine("invalid choice try again");
                        break;

                }
                Console.WriteLine();
            } while (choice != 0);
        }

    }
}
